<?php

use Faker\Generator as Faker;

$factory->define(TheProject\Group::class, function (Faker $faker) {
	return [
		'referencia' => $faker->unique()->randomElement([ 'supermario', 'developers' ]),
	];
});
