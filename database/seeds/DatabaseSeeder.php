<?php

use Illuminate\Database\Seeder;
use TheProject\Group;
use TheProject\User;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$supermario = factory(Group::class)->create([ 'referencia' => 'supermario' ]);
		$developers = factory(Group::class)->create([ 'referencia' => 'developers' ]);

		factory(User::class)->create([
			'name'     => 'Andres Carrillo',
			'email'    => 'andresjch2804@gmail.com',
			'username' => 'ajcarrillo',
			'password' => bcrypt('Parangaricutirimicuaro'),
		])->each(function ($user) use ($supermario, $developers) {
			$user->groups()->save($supermario);
			$user->groups()->save($developers);
		});

		factory(User::class)->create([
			'name'     => 'Víctor Alejandro Yah González',
			'email'    => 'victor.seq.sistemas@gmail.com',
			'username' => 'vyah',
			'password' => bcrypt('secret'),
		])->each(function ($user) use ($supermario, $developers) {
			$user->groups()->save($supermario);
			$user->groups()->save($developers);
		});

		factory(User::class)->create([
			'name'     => 'Josymar Santos',
			'email'    => 'josymar.santos2@gmail.com',
			'username' => 'jsantos',
			'password' => bcrypt('secret'),
		])->each(function ($user) use ($developers) {
			$user->groups()->save($developers);
		});

		factory(User::class)->create([
			'name'     => 'Julio Gorocica',
			'email'    => 'eduar881@gmail.com',
			'username' => 'jgorocica',
			'password' => bcrypt('secret'),
		])->each(function ($user) use ($developers) {
			$user->groups()->save($developers);
		});

		factory(User::class)->create([
			'name'     => 'Carlos Levi Ricalde',
			'email'    => 'leviricalde@gmail.com',
			'username' => 'lewis',
			'password' => bcrypt('secret'),
		])->each(function ($user) use ($developers) {
			$user->groups()->save($developers);
		});
	}
}
