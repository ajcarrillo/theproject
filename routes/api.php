<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
| url: /api
|
*/

Route::group([ 'middleware' => [ 'auth:api' ], 'prefix' => '/v1' ], function () {
	Route::get('/usuario-datos', [
		'uses' => 'Api\UsuarioDatoController@usuarioDatos',
		'as'   => 'usuario.datos',
	]);

	Route::get('/centros-de-trabajo', [
		'uses' => 'Api\EscuelaController@escuelas',
		'as'   => 'escuelas',
	]);

	Route::get('/centros-de-trabajo/{centroTrabajo}/responsable', [
		'uses' => 'Api\EscuelaController@getResponsable',
		'as'   => 'escuelas.getResponsable',
	]);

	/*Route::get('/centros-de-trabajo/plantilla', [
		'uses' => 'Api\EscuelaController@plantilla',
		'as'   => 'centros.trabajo.plantilla',
	]);*/

	Route::get('/centros-de-trabajo/plantilla', [
		'uses' => 'Api\PersonaCtController@plantilla',
		'as'   => 'centros.trabajo.plantilla',
	]);
});