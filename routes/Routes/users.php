<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 25/09/17
 * Time: 13:45
 */


Route::get('/', [
	'uses' => 'Users\UserController@index',
	'as'   => 'users.index',
]);

Route::get('/nuevo', [
	'uses' => 'Users\UserController@create',
	'as'   => 'users.create',
]);

Route::get('/{user}', [
	'uses' => 'Users\UserController@edit',
	'as'   => 'users.edit',
]);

Route::put('/{user}', [
	'uses' => 'Users\UserController@update',
	'as'   => 'users.update',
]);

