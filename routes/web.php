<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([ 'middleware' => [ 'auth', 'role:supermario' ], 'prefix' => '/users' ], function () {
	require(__DIR__ . '/Routes/users.php');
});

Route::group([ 'middleware' => [ 'auth', 'role:developers' ], 'prefix' => '/api-resources' ], function () {
	Route::get('/', [
		'uses' => 'Resources\ResourceController@apiResources',
		'as'   => 'api.resources',
	]);
});

Route::group([ 'middleware' => [ 'auth', 'role:developers' ], 'prefix' => '/profile' ], function () {
	Route::get('/', function () {
		return view('developers.perfil');
	})->name('perfil');
});
