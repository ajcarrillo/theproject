<?php

namespace TheProject\Models\PortalDocente;

use Illuminate\Database\Eloquent\Model;
use function Sodium\crypto_box_publickey_from_secretkey;
use TheProject\Models\Usuarios\UsuarioDato;

class Comprobante extends Model
{
	protected $connection = 'portal_docente';
	protected $table      = 'comprobantes';
	protected $primaryKey = 'id';
	protected $appends    = [ 'categoriaPago' ];

	public function usuarioDato()
	{
		return $this->belongsTo(UsuarioDato::class, 'rfc', 'rfc');
	}

	public function getCategoriaPagoAttribute()
	{
		$result = explode('.', substr($this->plaza, 6));
		return $result[0];
	}

	public function toArray()
	{
		return [
			'plaza'          => $this->plaza,
			'categoria_pago' => $this->categoriaPago,
		];
	}
}
