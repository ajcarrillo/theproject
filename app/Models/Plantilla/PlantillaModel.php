<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 22/09/17
 * Time: 18:15
 */

namespace TheProject\Models\Plantilla;


use Illuminate\Database\Eloquent\Model;

class PlantillaModel extends Model
{
	protected $connection = 'plantilla';
}