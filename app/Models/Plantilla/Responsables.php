<?php

namespace TheProject\Models\Plantilla;

use TheProject\Models\Usuarios\UsuarioDato;

class Responsables extends PlantillaModel
{
    protected $table = 'rh_responsables';
    protected $primaryKey = 'id';

	public function centroTrabajo()
	{
		return $this->belongsTo(Escuela::class, 'cct', 'clavecct')
			->where('turno_id', $this->turno);
    }

	public function datosResponsable()
	{
		return $this->belongsTo(UsuarioDato::class, 'rfc_responsable_ct', 'rfc');
    }
}
