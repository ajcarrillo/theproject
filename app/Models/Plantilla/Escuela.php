<?php

namespace TheProject\Models\Plantilla;

use TheProject\Models\Filters\Filterable;
use TheProject\Models\Geoestadistica\NivelEd;

class Escuela extends PlantillaModel
{
	use Filterable;

	protected $table      = 'rh_cat_escuelas';
	protected $primaryKey = 'id';
	protected $appends    = [ 'nivelEducativo', 'nivelEducativoDescripcion', 'fullName', 'nivelEd' ];

	public function escuelaTurno()
	{
		return $this->belongsTo(Turno::class, 'turno', 'id');
	}

	public function plantilla()
	{
		return $this->hasMany(PersonaCtDatos::class, 'cct', 'clavecct')
			->with([ 'tUsuario', 'tUsuario.usuarioDato' ])
			->where('turno_id', $this->escuelaTurno->id);
	}

	public function responsable()
	{
		return $this->belongsTo(Responsables::class, 'clavecct', 'cct')
			->where('turno', $this->escuelaTurno->id);
	}

	public function getNivelEducativoAttribute()
	{
		return substr($this->clavecct, 2, 3);
	}

	public function getNivelEducativoDescripcionAttribute()
	{
		$nivelEd = NivelEd::where('CVENIVEL', $this->nivelEducativo)->first();

		return $nivelEd->NIVEL . ' ' . $nivelEd->MODGRAL;
	}

	public function getFullNameAttribute()
	{
		return $this->nombrect . ' ' . $this->escuelaTurno->abreviatura;
	}

	public function getNivelEdAttribute()
	{
		return NivelEd::where('CVENIVEL', $this->nivelEducativo)->first();
	}

	public function toArray()
	{
		return [
			'id'              => $this->id,
			'clavecct'        => $this->clavecct,
			'nombrect'        => $this->nombrect,
			'nombre_completo' => $this->fullName,
			'turno'           => $this->escuelaTurno,
			'nivel_ed'        => $this->nivelEd,
			'zona_escolar'    => $this->zonaescola,
			'supervisor'      => $this->nombre_supervisor,
		];
	}
}
