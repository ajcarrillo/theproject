<?php

namespace TheProject\Models\Plantilla;

class Turno extends PlantillaModel
{
	protected $table      = 'rh_ct_turnos';
	protected $primaryKey = 'id';

	public function toArray()
	{
		return [
			'id'          => $this->id,
			'abreviatura' => $this->abreviatura,
			'nombre'      => $this->nombre,
		];
	}
}
