<?php

namespace TheProject\Models\Plantilla;


class PersonaCtAsignatura extends PlantillaModel
{
    protected $table = 'rh_persona_ct_asignatura';
    protected $primaryKey = 'id';

	public function personaCT()
	{
		return $this->belongsTo(PersonaCt::class, 'persona_ct', 'id');
    }

	public function asignatura()
	{
		return $this->belongsTo(Asignatura::class, 'asignatura_id', 'id');
    }
}
