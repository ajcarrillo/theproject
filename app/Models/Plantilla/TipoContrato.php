<?php

namespace TheProject\Models\Plantilla;


class TipoContrato extends PlantillaModel
{
	protected $table      = 'rh_tipo_contratacion';
	protected $primaryKey = 'id';

	public function personasCt()
	{
		return $this->hasMany(PersonaCt::class, 'tipo_contratacion');
	}

	public function toArray()
	{
		return [
			'id'          => $this->id,
			'descripcion' => $this->nombre,
			'siglas'      => $this->siglas,
		];
	}
}
