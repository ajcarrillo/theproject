<?php

namespace TheProject\Models\Plantilla;


class Funcion extends PlantillaModel
{
	protected $table      = 'rh_plantilla_funciones_nivel';
	protected $primaryKey = 'id';

	public function personasCt()
	{
		return $this->hasMany(PersonaCt::class, 'funcion_id');
	}

	public function toArray()
	{
		return [
			'id'          => $this->id,
			'descripcion' => $this->nombre,
			'nivel'       => $this->nivel,
			'tipo'        => $this->tipo,
		];
	}
}
