<?php

namespace TheProject\Models\Plantilla;


class Estatus extends PlantillaModel
{
	protected $table      = 'rh_empleado_estatus';
	protected $primaryKey = 'id';

	public function personasCt()
	{
		return $this->hasMany(PersonaCt::class, 'estatus_contrato');
	}

	public function toArray()
	{
		return [
			'id'          => $this->id,
			'descripcion' => $this->nombre,
		];
	}
}
