<?php

namespace TheProject\Models\Plantilla;

use Illuminate\Database\Eloquent\Model;
use TheProject\Models\Filters\Filterable;
use TheProject\Models\Usuarios\Usuario;

class PersonaCt extends PlantillaModel
{
	use Filterable;

	protected $table      = 'rh_persona_ct';
	protected $primaryKey = 'id';

	public function tUsuario()
	{
		return $this->belongsTo(Usuario::class, 'persona', "cruge_user");
	}

	public function funcion()
	{
		return $this->belongsTo(Funcion::class, 'funcion_id');
	}

	public function tipoContratacion()
	{
		return $this->belongsTo(TipoContrato::class, 'tipo_contratacion');
	}

	public function estatusContrato()
	{
		return $this->belongsTo(Estatus::class, 'estatus_contrato');
	}

	public function asignaturas()
	{
		return $this->hasMany(PersonaCtAsignatura::class, 'persona_ct', 'id');
	}

	public function toArray()
	{
		return [
			"id"               => $this->id,
			"persona"          => $this->persona,
			"cct"              => $this->cct,
			"qna_ini"          => $this->qna_ini,
			"qna_fin"          => $this->qna_fin,
			"funcion"          => $this->funcion,
			"tipoContratacion" => $this->tipoContratacion,
			"estatusContrato"  => $this->estatusContrato,
		];
	}
}
