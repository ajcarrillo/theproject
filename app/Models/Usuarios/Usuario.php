<?php

namespace TheProject\Models\Usuarios;


use TheProject\Models\Filters\Filterable;
use TheProject\Models\Plantilla\PersonaCt;

class Usuario extends UsuarioModel
{
	use Filterable;

	protected $table      = 't_usuario';
	protected $primaryKey = 'id';

	public function usuarioDato()
	{
		return $this->hasOne(UsuarioDato::class, 'usuario', 'id');
	}

	public function crugeUser()
	{
		return $this->belongsTo(CrugeUser::class, 'cruge_user', 'iduser');
	}

	public function personasCt()
	{
		return $this->hasMany(PersonaCt::class, 'persona', 'cruge_user');
	}

	/*public function toArray()
	{
		return [
			"id"         => $this->id,
			"cruge_user" => $this->cruge_user,
			"rfc_temp"   => $this->rfc_temp,
		];
	}*/
}
