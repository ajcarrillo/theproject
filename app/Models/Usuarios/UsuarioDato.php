<?php

namespace TheProject\Models\Usuarios;


use TheProject\Models\Filters\Filterable;
use TheProject\Models\PortalDocente\Comprobante;

class UsuarioDato extends UsuarioModel
{
	use Filterable;

	protected $table      = 't_usuario_datos';
	protected $primaryKey = 'usuario';
	protected $appends    = [ 'foto', 'sexo' ];

	public function tUsuario()
	{
		return $this->belongsTo(Usuario::class, 'usuario', 'id');
	}

	public function comprobantes()
	{
		return $this->hasMany(Comprobante::class, 'rfc', 'rfc');
	}

	public function plazas()
	{
		return $this->hasMany(Comprobante::class, 'rfc', 'rfc')
			->selectRaw('plaza')
			->groupBy('plaza');
	}

	public function getFotoAttribute()
	{
		return "http://sarh.seq.gob.mx/fotos/" . $this->rfc . ".jpg";
	}

	public function getSexoAttribute()
	{
		return substr($this->curp, 10, 1);
	}

	/*public function toArray()
	{
		return [
			'usuario'                      => $this->usuario,
			'rfc'                          => $this->rfc,
			'curp'                         => $this->curp,
			'primer_apellido'              => $this->primer_apellido,
			'segundo_apellido'             => $this->segundo_apellido,
			'nombre'                       => $this->nombre,
			'no_nomina'                    => $this->no_nomina,
			'telefono'                     => $this->telefono,
			'clave_enomina'                => $this->clave_enomina,
			'telefono_particular'          => $this->telefono_particular,
			'telefono_celular'             => $this->telefono_celular,
			'domicilio_calle'              => $this->domicilio_calle,
			'domicilio_numero_exterior'    => $this->domicilio_numero_exterior,
			'domicilio_colonia'            => $this->domicilio_colonia,
			'domicilio_codigo_postal'      => $this->domicilio_codigo_postal,
			'domicilio_municipio'          => $this->domicilio_municipio,
			'domicilio_entidad_federativa' => $this->domicilio_entidad_federativa,
			'foto'                         => $this->foto,
			't_usuario'                    => $this->tUsuario,
			'cruge'                        => $this->tUsuario->crugeUser,
			'adscripciones'                => $this->tUsuario->personasCt,
			'plazas'                       => $this->plazas,
		];
	}*/
}
