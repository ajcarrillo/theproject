<?php

namespace TheProject\Models\Usuarios;

use Illuminate\Database\Eloquent\Model;
use TheProject\Models\Filters\Filterable;

class CrugeUser extends UsuarioModel
{
	use Filterable;

	protected $table      = 'cruge_user';
	protected $primaryKey = 'iduser';
	protected $hidden     = [ 'password', ];

	public function usuario()
	{
		return $this->hasOne(Usuario::class, 'cruge_user', 'iduser');
	}

	public function toArray()
	{
		return [
			'iduser'   => $this->iduser,
			'username' => $this->username,
			'email'    => $this->email,
		];
	}
}
