<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 22/09/17
 * Time: 18:16
 */

namespace TheProject\Models\Usuarios;


use Illuminate\Database\Eloquent\Model;

class UsuarioModel extends Model
{
	protected $connection = 'usuarios';
}