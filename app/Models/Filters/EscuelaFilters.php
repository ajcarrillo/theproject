<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 05/10/17
 * Time: 01:12
 */

namespace TheProject\Models\Filters;


class EscuelaFilters extends QueryFilters
{

	public function cct($cct)
	{
		return $this->builder->where('clavecct', $cct);
	}

}