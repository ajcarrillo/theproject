<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 22/09/17
 * Time: 19:43
 */

namespace TheProject\Models\Filters;


class EmpleadoFilters extends QueryFilters
{

	public function rfc($rfc)
	{
		return $this->builder->where('rfc', $rfc);
	}

	public function curp($curp)
	{
		return $this->builder->where('curp', $curp);
	}

	public function primer_apellido($primer_apellido)
	{
		return $this->builder->where('primer_apellido', $primer_apellido);
	}

	public function segundo_apellido($segundo_apellido)
	{
		return $this->builder->where('segundo_apellido', $segundo_apellido);
	}

	public function nombre($nombre)
	{
		return $this->builder->where('nombre', $nombre);
	}

}