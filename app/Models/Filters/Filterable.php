<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 19/07/17
 * Time: 09:59
 */

namespace TheProject\Models\Filters;

use Illuminate\Database\Eloquent\Builder;

trait Filterable
{
    /**
     * Filter a result set.
     *
     * @param  Builder $query
     * @param  QueryFilters $filters
     * @return Builder
     */
    public function scopeFilter($query, QueryFilters $filters)
    {
        return $filters->apply($query);
    }
}
