<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 05/10/17
 * Time: 01:01
 */

namespace TheProject\Models\Geoestadistica;

use Illuminate\Database\Eloquent\Model;

class GeoEstadisticaModel extends Model
{
	protected $connection = 'geoestadisticadb';
}