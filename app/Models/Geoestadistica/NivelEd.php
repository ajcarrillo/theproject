<?php

namespace TheProject\Models\Geoestadistica;

class NivelEd extends GeoEstadisticaModel
{
    protected $table = 'nivel_ed';
    protected $primaryKey = 'ID_NIVEL_ED';
}
