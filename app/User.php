<?php

namespace TheProject;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

	protected $appends = ['groupsValues'];

	public function groups()
	{
		return $this->belongsToMany(Group::class);
	}
	public function hasRole($role)
	{
		return $this->groups()->where('referencia', $role)->count();
	}

	public function getGroupsValuesAttribute()
	{
		$groups = $this->groups()->get();

		return $groups->implode('referencia', ', ');

	}
}
