<?php

namespace TheProject;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
	protected $table = 'groups';
	protected $fillable = [
		'referencia',
	];

	public function users()
	{
		return $this->belongsToMany(User::class);
	}
}
