<?php

namespace TheProject\Http\Controllers\Api;

use Illuminate\Http\Request;
use TheProject\Http\Controllers\Controller;
use TheProject\Models\Plantilla\Escuela;
use TheProject\Models\Plantilla\PersonaCtDatos;

class PersonaCtController extends Controller
{
	public function plantilla(Request $request)
	{
		$cct   = $request->query('cct');
		$turno = $request->query('turno');

		$centroTrabajo = Escuela::where('clavecct', $cct)->where('turno', $turno)->first();

		$query = PersonaCtDatos::with([ 'tUsuario', 'tUsuario.usuarioDato' => function ($usuarioDato) {
			return $usuarioDato->select([ 'usuario', 'rfc', 'curp', 'primer_apellido', 'segundo_apellido', 'nombre' ]);
		} ])
			->where('cct', $cct)
			->where('turno_id', $turno)
			->where('estatus_contrato', 2);

		if ($centroTrabajo->nivelEd->ID_NIVEL == 'E') {
			$query = $query->groupBy('persona');
		}

		$plantilla = $query->get([ 'id', 'persona', 'cct' ]);

		$total = $plantilla->count();

		return [ 'data' => compact('centroTrabajo', 'plantilla', 'total') ];
	}
}
