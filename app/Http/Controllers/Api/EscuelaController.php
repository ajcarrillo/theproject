<?php

namespace TheProject\Http\Controllers\Api;

use Illuminate\Http\Request;
use TheProject\Http\Controllers\Controller;
use TheProject\Http\Resources\EscuelaCollection;
use TheProject\Models\Filters\EscuelaFilters;
use TheProject\Models\Plantilla\Escuela;
use TheProject\Models\Plantilla\PersonaCt;

class EscuelaController extends Controller
{
	public function escuelas(EscuelaFilters $filters)
	{
		foreach ($filters->filters() as $name => $value) {
			if ($value == NULL) {
				return response()
					->json([ 'data' => [],
					         'meta' => [
						         'status' => 400,
						         'msg'    => 'Bad request',
						         'errors' => [ 'El filtro ' . $name . ' es nulo', 'Hey dude, what are you doing? Don\'t mess with me! (ง •̀_•́)ง' ],
					         ],
					], 400);
			}
		}

		if ($filters->filters() == [] || array_has($filters->filters(), 'page')) {
			return new EscuelaCollection(Escuela::with('escuelaTurno')->filter($filters)->paginate(20));
		}

		return new EscuelaCollection(Escuela::with('escuelaTurno')
			->filter($filters)->get());
	}

	public function escuela(EscuelaFilters $filters)
	{

	}

	public function plantilla(Request $request)
	{
		$cct   = $request->query('cct');
		$turno = $request->query('turno');

		$centroTrabajo = Escuela::where('clavecct', $cct)->where('turno', $turno)->first();

		if ($centroTrabajo->nivelEd->ID_NIVEL == 'E') {
			$plantilla = $centroTrabajo->plantilla()->groupBy('persona')
				->get();
		} else {
			$plantilla = $centroTrabajo->plantilla;
		}

		$total = $plantilla->count();

		return [ 'data' => compact('centroTrabajo', 'plantilla', 'total') ];
	}

	public function getResponsable(Escuela $centroTrabajo)
	{
		$centroTrabajo->loadMissing([ 'responsable' ]);

		$responsable = $centroTrabajo->responsable;
		$responsable->datosResponsable;

		return [ 'data' => compact('responsable', 'usuario'), 'meta' => [
			'status' => 200,
			'msg'    => 'OK',
			'errors' => [],
		], ];
	}
}
