<?php

namespace TheProject\Http\Controllers\Api;

use TheProject\Http\Controllers\Controller;
use TheProject\Http\Resources\UsuarioDatoCollection;
use TheProject\Models\Filters\EmpleadoFilters;
use TheProject\Models\Usuarios\UsuarioDato;

class UsuarioDatoController extends Controller
{
	public function usuarioDatos(EmpleadoFilters $filters)
	{
		foreach ($filters->filters() as $name => $value) {
			if ($value == NULL) {
				return response()
					->json([ 'data' => [],
					         'meta' => [
						         'status' => 400,
						         'msg'    => 'Bad request',
						         'errors' => [ 'El filtro ' . $name . ' es nulo', 'Hey dude, what are you doing? Don\'t mess with me! (ง •̀_•́)ง' ],
					         ],
					], 400);
			}
		}

		if ($filters->filters() == [] || array_has($filters->filters(),'page')) {
			return new UsuarioDatoCollection(UsuarioDato::with(['tUsuario', 'tUsuario.crugeUser', 'tUsuario.personasCt'])->filter($filters)->paginate(20));
		}

		return new UsuarioDatoCollection(UsuarioDato::with([
			'tUsuario',
			'tUsuario.crugeUser',
			'tUsuario.personasCt',
		])->filter($filters)->get());
	}
}
