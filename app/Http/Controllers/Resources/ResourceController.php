<?php

namespace TheProject\Http\Controllers\Resources;

use Illuminate\Http\Request;
use TheProject\Http\Controllers\Controller;

class ResourceController extends Controller
{
	public function apiResources()
	{
		return view('api_resources.home');
    }
}
