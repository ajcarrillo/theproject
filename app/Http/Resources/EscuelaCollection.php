<?php

namespace TheProject\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EscuelaCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
	    return [
		    'data' => $this->collection,
		    'meta' => [
			    'status' => 200,
			    'msg'    => 'OK',
			    'errors' => [],
		    ],
	    ];
    }
}
