<?php

namespace TheProject\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UsuarioDatoResource extends Resource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request
	 * @return array
	 */
	public function toArray($request)
	{
		return [
			'id'                           => $this->usuario,
			'rfc'                          => $this->rfc,
			'curp'                         => $this->curp,
			'primer_apellido'              => $this->primer_apellido,
			'segundo_apellido'             => $this->segundo_apellido,
			'nombre'                       => $this->nombre,
			'no_nomina'                    => $this->no_nomina,
			'telefono'                     => $this->telefono,
			'clave_enomina'                => $this->clave_enomina,
			'telefono_particular'          => $this->telefono_particular,
			'telefono_celular'             => $this->telefono_celular,
			'domicilio_calle'              => $this->domicilio_calle,
			'domicilio_numero_exterior'    => $this->domicilio_numero_exterior,
			'domicilio_colonia'            => $this->domicilio_colonia,
			'domicilio_codigo_postal'      => $this->domicilio_codigo_postal,
			'domicilio_municipio'          => $this->domicilio_municipio,
			'domicilio_entidad_federativa' => $this->domicilio_entidad_federativa,
			'foto'                         => $this->foto,
			'cruge'                        => $this->tUsuario->crugeUser,
			'adscripciones'                => $this->tUsuario->personasCt,
		];
	}
}
