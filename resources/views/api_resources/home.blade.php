@extends('layouts.app')

@section('extra-head')

@endsection

@section('content')

    <div class="content">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2"></div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <h1>SEQ API</h1>

                <ol class="breadcrumb">
                    <li><a href="#api-empleados">Métodos de Empleado</a></li>
                    <li><a href="#api-escuelas">Métodos de Centro de Trabajo</a></li>
                </ol>

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">URI Structure</h3>
                    </div>
                    <div class="panel-body">
                        <p>Todas las peticiones al API empiezan con <code>http://api.seyc.app</code> el siguiente segmento de la URI depende del tipo de request:</p>
                        <ul>
                            <li>
                                <strong>Empleado: </strong>obtiene la información del empleado
                                <p><code>http://api.seyc.app/api/v1/usuario-datos</code></p>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Response Format</h3>
                    </div>
                    <div class="panel-body">
                        <p>La API devuelve objetos JSON (content-type: application/json). La respuesta varía de acuerdo al método usado, pero toda respuesta contiene la
                            siguiente estructura:</p>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>meta</th>
                                    <td>
                                        El objeto <code>meta</code> coincide con el mensaje de la respuesta HTTP:
                                        <ul class="tight">
                                            <li><code>status</code>: El código de estado de HTTP (ejem, <code>200</code>)</li>
                                            <li><code>msg</code>: El mensaje HTTP (ejem, <code>OK</code>)</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <th>data</th>
                                    <td>Resultados especificos del API</td>
                                </tr>
                            </tbody>
                        </table>
                        <h4>Ejemplo</h4>
                        <pre class="language-javascrip"><code class="language-javascript">{
    "data": { ... },
    "meta":{
        "status": 200,
        "msg": "OK",
        "errors": [...]
    }
}</code></pre>
                        <h4>Paginación</h4>
                        <p>En algunos casos la API tiene la característica de paginar la respuesta, con la finalidad de no sobre cargar el motor de datos.</p>
                        <p><strong>El número de elementos por página es 20</strong></p>

                        <h3>Respuesta</h3>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Campo</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>current_page</td>
                                    <td>Integer</td>
                                    <td>Representa la página actual</td>
                                </tr>
                                <tr>
                                    <td>from</td>
                                    <td>Integer</td>
                                    <td>Representa a partir de que elemento iniciar la paginación</td>
                                </tr>
                                <tr>
                                    <td>last_page</td>
                                    <td>Integer</td>
                                    <td>Representa cual es la última página</td>
                                </tr>
                                <tr>
                                    <td>path</td>
                                    <td>URL</td>
                                    <td>Inidica la URL base</td>
                                </tr>
                                <tr>
                                    <td>per_page</td>
                                    <td>Integer</td>
                                    <td>Representa cuantos elementos hay por página</td>
                                </tr>
                                <tr>
                                    <td>to</td>
                                    <td>Integer</td>
                                    <td>Representa el elemento final de la página</td>
                                </tr>
                                <tr>
                                    <td>total</td>
                                    <td>Integer</td>
                                    <td>Representa el total de elementos.</td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-center"><strong>LINKS</strong></td>
                                </tr>
                                <tr>
                                    <td>first</td>
                                    <td>URL</td>
                                    <td>Representa la URL para obtener la primera página</td>
                                </tr>
                                <tr>
                                    <td>last</td>
                                    <td>URL</td>
                                    <td>Representa la URL para obtener la última página</td>
                                </tr>
                                <tr>
                                    <td>prev</td>
                                    <td>URL</td>
                                    <td>Representa la URL para obtener la página anterior</td>
                                </tr>
                                <tr>
                                    <td>next</td>
                                    <td>URL</td>
                                    <td>Representa la URL para obtener la página siguiente</td>
                                </tr>
                            </tbody>
                        </table>
                        <pre class="language-javascript"><code class="language-javascript">"meta": {
    "status": 200,
    "msg": "OK",
    "errors": [],
    "current_page": 1,
    "from": 1,
    "last_page": 9639,
    "path": "http://api.seyc.app/api/v1/usuario-datos",
    "per_page": 2,
    "to": 2,
    "total": 19278
},
"links": {
    "first": "http://api.seyc.app/api/v1/usuario-datos?page=1",
    "last": "http://api.seyc.app/api/v1/usuario-datos?page=9639",
    "prev": null,
    "next": "http://api.seyc.app/api/v1/usuario-datos?page=2"
}</code></pre>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">About the API Documentation</h3>
                    </div>
                    <div class="panel-body">
                        <p>La documentación incluye algunos ejemplos prácticos. Por favor, haga clic en ellos, pero no utilice la clave de API
                            incrustada para sus propios propósitos nefastos. Registre su aplicación para obtener su propia API Token.</p>
                        <h4>URI Conventions</h4>
                        <table class="table">
                            <tbody>
                                <tr class="header">
                                    <th>Notación</th>
                                    <th>Significado</th>
                                    <th>Ejemplo</th>
                                </tr>
                                <tr>
                                    <th>Curly brackets {&nbsp;}</th>
                                    <td>Elemento requerido</td>
                                    <td><code>api.seyc.app/api/v1/usuario-datos?curp=<br>{curp}</code><br>
                                        <div class="note">La curp es requerida</div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Square&nbsp;brackets&nbsp;[&nbsp;]</th>
                                    <td>Elemento opcional</td>
                                    <td><code>api.seyc.app/api/v1/usuario-datos[?curp=<br>{curp}]</code><br>
                                        <div class="note">Especificar la curp es opcional</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title" id="auth">Authentication</h3>
                    </div>
                    <div class="panel-body">
                        <p>La API utiliza un método de autenticación:</p>
                        <ul>
                            <li>
                                <strong>API token</strong> Requiere un API token.
                                <ol>
                                    <li>Ejemplo:

                                        <pre class="language-json"><code class="language-javascript">"headers": {
    "accept": "application/json",
    "authorization": "Bearer $2y$10$B5VquEu8Yw1GP3pa1YsYEu1fwFFt.CQbQ7AeRhPwMO7U3yixMY81u"
}</code></pre>
                                    </li>
                                    <li>Ver tu API Token <a href="{{ route('perfil') }}">aquí.</a></li>
                                </ol>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">¿Cómo realizar la petición?</h3>
                    </div>
                    <div class="panel-body">
<pre class="language-javascript"><code class="language-javascript">var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://api.seyc.app/api/v1/usuario-datos?curp=YAGV810728HQRHNC04",
  "method": "GET",
  "headers": {
    "accept": "application/json",
    "authorization": "Bearer $2y$10$B5VquEu8Yw1GP3pa1YsYEu1fwFFt.CQbQ7AeRhPwMO7U3yixMY81u",
  }
}

$.ajax(settings).done(function (response) {
  console.log(response);
});</code></pre>
                    </div>
                </div>

                <div class="panel panel-primary" id="api-empleados">
                    <div class="panel-heading">
                        <h2 class="panel-title">Métodos de Empleado</h2>
                    </div>
                    <div class="panel-body">
                        <h3><code>/api/v1/usuario-dato</code> - Obtiene una colección de empleados</h3>

                        <h5>Ejemplos</h5>
                        <ul>
                            <li><code>http://api.seyc.app/api/v1/usuario-datos</code></li>
                        </ul>

                        <h4>Método</h4>
                        <table class="table">
                            <tbody>
                                <tr class="header">
                                    <th>URI</th>
                                    <th>HTTP&nbsp;Method</th>
                                    <th>Authentication</th>
                                </tr>
                                <tr>
                                    <td>
                                        <code>http://api.seyc.app/api/v1/usuario-datos[?curp={curp}&rfc={rfc}&nombre={nombre}&primer_apellido={primer_apellido}&segundo_apellido={segundo_apellido}]</code>
                                    </td>
                                    <td>GET</td>
                                    <td><a href="#auth">API Token</a></td>
                                </tr>
                            </tbody>
                        </table>

                        <h4>Parámetros</h4>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Parámetros</th>
                                    <th>Tipo</th>
                                    <th>Default</th>
                                    <th>¿Requerido?</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>nombre</td>
                                    <td>String</td>
                                    <td>N/A</td>
                                    <td>No</td>
                                </tr>
                                <tr>
                                    <td>primer_apellido</td>
                                    <td>String</td>
                                    <td>N/A</td>
                                    <td>No</td>
                                </tr>
                                <tr>
                                    <td>segundo_apellido</td>
                                    <td>String</td>
                                    <td>N/A</td>
                                    <td>No</td>
                                </tr>
                                <tr>
                                    <td>curp</td>
                                    <td>String</td>
                                    <td>N/A</td>
                                    <td>No</td>
                                </tr>
                                <tr>
                                    <td>rfc</td>
                                    <td>String</td>
                                    <td>N/A</td>
                                    <td>No</td>
                                </tr>
                            </tbody>
                        </table>

                        <h4>Ejemplos</h4>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <code data-toggle="tooltip" title="Haz click para mostrar la respuesta">http://api.seyc.app/api/v1/usuario-datos</code>
                                </h3>
                            </div>
                            <div class="panel-body">
<pre class="language-javascript"><code>{
    "data": [
        {
            "id": 1,
            "rfc": "TUKB630426QV2",
            "curp": "TUKB630426HQRZLR06",
            "primer_apellido": "TUZ",
            "segundo_apellido": "KAUIL",
            "nombre": "BRAULIO",
            "no_nomina": 15876,
            "telefono": null,
            "clave_enomina": "15876",
            "telefono_particular": null,
            "telefono_celular": "(983) 833-6413",
            "domicilio_calle": null,
            "domicilio_numero_exterior": null,
            "domicilio_colonia": null,
            "domicilio_codigo_postal": null,
            "domicilio_municipio": null,
            "domicilio_entidad_federativa": null,
            "foto": "http://sarh.seq.gob.mx/fotos/TUKB630426QV2.jpg",
            "cruge": {
                "username": "TUKB630426QV2",
                "email": "brauliotuzkauil@hotmail.com"
            },
            "adscripciones": [],
            "plazas": [
                {
                    "plaza": "072313CF3480700.0300059"
                },
                {
                    "plaza": "072314ED0281000.0300004"
                }
            ]
        },
        {
            "id": 2,
            "rfc": "SIBA351015LJ9",
            "curp": "SIBA351015MQRLCB03",
            "primer_apellido": "SILVA",
            "segundo_apellido": "BECERRA",
            "nombre": "ABIGAIL RAHAB",
            "no_nomina": 1,
            "telefono": "",
            "clave_enomina": "1",
            "telefono_particular": null,
            "telefono_celular": "(987) 107-0381",
            "domicilio_calle": null,
            "domicilio_numero_exterior": null,
            "domicilio_colonia": null,
            "domicilio_codigo_postal": null,
            "domicilio_municipio": null,
            "domicilio_entidad_federativa": null,
            "foto": "http://sarh.seq.gob.mx/fotos/SIBA351015LJ9.jpg",
            "cruge": {
                "username": "SIBA351015LJ9",
                "email": "a_silvabecerra@hotmail.com"
            },
            "adscripciones": [
                {
                    "persona": 5,
                    "cct": "23FZP0003L",
                    "qna_ini": null,
                    "qna_fin": null,
                    "funcion": {
                        "id": 317,
                        "descripcion": "SUPERVISOR O INSPECTOR",
                        "nivel": "FZP",
                        "tipo": "DOCENTE"
                    },
                    "tipoContratacion": {
                        "id": 1,
                        "descripcion": "Base",
                        "siglas": "BA"
                    },
                    "estatusContrato": {
                        "id": 2,
                        "descripcion": "Activo"
                    }
                }
            ],
            "plazas": [
                {
                    "plaza": "078321E010100.0230017"
                }
            ]
        }
    ],
    "meta": {
        "status": 200,
        "msg": "OK",
        "errors": [],
        "current_page": 1,
        "from": 1,
        "last_page": 9639,
        "path": "http://api.seyc.app/api/v1/usuario-datos",
        "per_page": 2,
        "to": 2,
        "total": 19278
    },
    "links": {
        "first": "http://api.seyc.app/api/v1/usuario-datos?page=1",
        "last": "http://api.seyc.app/api/v1/usuario-datos?page=9639",
        "prev": null,
        "next": "http://api.seyc.app/api/v1/usuario-datos?page=2"
    }
}</code></pre>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <code data-toggle="tooltip" title="Haz click para mostrar la respuesta">http://api.seyc.app/api/v1/usuario-datos?curp=YAGV810728HQRHNC04</code>
                                </h3>
                            </div>
                            <div class="panel-body">
<pre class="language-javascript"><code class="language-javascript">{
    "data": [
        {
            "id": 10659,
            "rfc": "YAGV8107286J7",
            "curp": "YAGV810728HQRHNC04",
            "primer_apellido": "YAH",
            "segundo_apellido": "GONZALEZ",
            "nombre": "VICTOR ALEJANDRO",
            "no_nomina": 11205,
            "telefono": "",
            "clave_enomina": "11205",
            "telefono_particular": null,
            "telefono_celular": "(983) 124-0930",
            "domicilio_calle": null,
            "domicilio_numero_exterior": null,
            "domicilio_colonia": null,
            "domicilio_codigo_postal": null,
            "domicilio_municipio": null,
            "domicilio_entidad_federativa": null,
            "foto": "http://sarh.seq.gob.mx/fotos/YAGV8107286J7.jpg",
            "cruge": {
                "username": "YAGV8107286J7",
                "email": "ya_victor30@hotmail.com"
            },
            "adscripciones": [
                {
                    "persona": 3,
                    "cct": "23ADG0078N",
                    "qna_ini": null,
                    "qna_fin": null,
                    "funcion": {
                        "id": 34,
                        "descripcion": "JEFE DE DEPARTAMENTO",
                        "nivel": "ADG",
                        "tipo": "MANDO"
                    },
                    "tipoContratacion": {
                        "id": 3,
                        "descripcion": "Confianza",
                        "siglas": "CF"
                    },
                    "estatusContrato": {
                        "id": 2,
                        "descripcion": "Activo"
                    }
                }
            ],
            "plazas": [
                {
                    "plaza": "072313CF0105900.0300015"
                }
            ]
        }
    ],
    "meta": {
        "status": 200,
        "msg": "OK",
        "errors": []
    }
}</code></pre>
                            </div>
                        </div>

                    </div>
                </div>

                @include('api_resources.escuelas.escuelas')
            </div>
        </div>
    </div>

@endsection

@section('extra-script')

@endsection