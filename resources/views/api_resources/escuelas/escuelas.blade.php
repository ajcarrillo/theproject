<div class="panel panel-primary" id="api-escuelas">
    <div class="panel-heading">
        <h3 class="panel-title">Métodos de Centros de Trabajo</h3>
    </div>
    <div class="panel-body">
        <h3><code>/centros-de-trabajo</code> - Obtiene una colección de centros de trabajo</h3>

        <h5>Ejemplos</h5>
        <ul>
            <li><code>http://api.seyc.app/api/v1/centros-de-trabajo</code></li>
        </ul>

        <h4>Método</h4>
        <table class="table">
            <tbody>
                <tr class="header">
                    <th>URI</th>
                    <th>HTTP&nbsp;Method</th>
                    <th>Authentication</th>
                </tr>
                <tr>
                    <td>
                        <code>http://api.seyc.app/api/v1/centros-de-trabajo[?cct={cct}]</code>
                    </td>
                    <td>GET</td>
                    <td><a href="#auth">API Token</a></td>
                </tr>
            </tbody>
        </table>

        <h4>Parámetros</h4>
        <table class="table">
            <thead>
                <tr>
                    <th>Parámetros</th>
                    <th>Tipo</th>
                    <th>Default</th>
                    <th>¿Requerido?</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>cct</td>
                    <td>String</td>
                    <td>N/A</td>
                    <td>No</td>
                </tr>
            </tbody>
        </table>

        <h4>Ejemplos</h4>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <code data-toggle="tooltip" title="Haz click para mostrar la respuesta">http://api.seyc.app/api/v1/centros-de-trabajo</code>
                </h3>
            </div>
            <div class="panel-body">
                <pre data-src="{{ asset('/js/prism/centros_de_trabajo/centros_de_trabajo.json') }}" class="language-javascript"></pre>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <code data-toggle="tooltip" title="Haz click para mostrar la respuesta">http://api.seyc.app/api/v1/centros-de-trabajo?cct=23DPR0221S</code>
            </div>
            <div class="panel-body">
                <pre data-src="{{ asset('/js/prism/centros_de_trabajo/centro_trabajo.json') }}" class="language-javascript"></pre>
            </div>
        </div>

        <h3><code>/centros-de-trabajo/plantilla</code> - Obtiene el centro de trabajo y su plantilla activa</h3>

        <h4>Método</h4>
        <table class="table">
            <tbody>
                <tr class="header">
                    <th>URI</th>
                    <th>HTTP&nbsp;Method</th>
                    <th>Authentication</th>
                </tr>
                <tr>
                    <td>
                        <code>http://api.seyc.app/api/v1/centros-de-trabajo/plantilla{?cct={cct}&turno={turno}}</code>
                    </td>
                    <td>GET</td>
                    <td><a href="#auth">API Token</a></td>
                </tr>
            </tbody>
        </table>

        <h4>Parámetros</h4>
        <table class="table">
            <thead>
                <tr>
                    <th>Parámetros</th>
                    <th>Tipo</th>
                    <th>Default</th>
                    <th>¿Requerido?</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>cct</td>
                    <td>String</td>
                    <td>N/A</td>
                    <td>Si</td>
                </tr>
                <tr>
                    <td>turno</td>
                    <td>Integer</td>
                    <td>N/A</td>
                    <td>Si</td>
                </tr>
            </tbody>
        </table>

        <h4>Ejemplos</h4>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><code>http://api.seyc.app/api/v1/centros-de-trabajo/plantilla?cct=23DIN0033T&turno=4</code></h3>
            </div>
            <div class="panel-body">
                <pre data-src="{{ asset('/js/prism/centros_de_trabajo/plantilla.json') }}" class="language-javascript"></pre>
            </div>
        </div>

        <h3><code>/centros-de-trabajo/{cct_id}/responsable</code> - Obtiene el responsable del centro de trabajo</h3>

        <h4>Método</h4>
        <table class="table">
            <tbody>
                <tr class="header">
                    <th>URI</th>
                    <th>HTTP&nbsp;Method</th>
                    <th>Authentication</th>
                </tr>
                <tr>
                    <td>
                        <code>http://api.seyc.app/api/v1/centros-de-trabajo/{cct_id}/responsable</code>
                    </td>
                    <td>GET</td>
                    <td><a href="#auth">API Token</a></td>
                </tr>
            </tbody>
        </table>

        <h4>Parámetros</h4>
        <table class="table">
            <thead>
                <tr>
                    <th>Parámetros</th>
                    <th>Tipo</th>
                    <th>Default</th>
                    <th>¿Requerido?</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>cct_id</td>
                    <td>Integer</td>
                    <td>N/A</td>
                    <td>Si</td>
                </tr>
            </tbody>
        </table>

        <h4>Ejemplos</h4>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><code>http://api.seyc.app/api/v1/centros-de-trabajo/1255/responsable</code></h3>
            </div>
            <div class="panel-body">
                <pre data-src="{{ asset('/js/prism/centros_de_trabajo/responsable.json') }}" class="language-javascript"></pre>
            </div>
        </div>
    </div>
</div>