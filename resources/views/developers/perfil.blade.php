@extends('layouts.app')

@section('extra-head')

@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2"></div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <h1>{{ Auth::user()->name }}</h1>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Mi token</h3>
                    </div>
                    <div class="panel-body">
                        <pre><code>{{ Auth::user()->api_token }}</code></pre>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">¿Cómo realizar la petición?</h3>
                    </div>
                    <div class="panel-body">
<pre class="language-javascript"><code class="language-javascript">var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://api.seyc.app/api/v1/usuario-datos?curp=YAGV810728HQRHNC04",
  "method": "GET",
  "headers": {
    "accept": "application/json",
    "authorization": "Bearer $2y$10$B5VquEu8Yw1GP3pa1YsYEu1fwFFt.CQbQ7AeRhPwMO7U3yixMY81u",
  }
}

$.ajax(settings).done(function (response) {
  console.log(response);
});</code></pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-script')

@endsection