@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Nuevo</div>

                    <div class="panel-body">
                        {!! Form::open(['class'=>'form-horizontal', 'method'=>'POST', 'route'=>'register']) !!}
                        @include('users._user_form')
                        @include('users._user_password')
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Guardar
                                </button>
                                <a href="{{ route('users.index') }}" class="btn btn-default pull-right">Regresar</a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
