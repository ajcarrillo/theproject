<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'Nombre:', ['class'=>'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', NULL, ['class'=>'form-control', 'required']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
    {!! Form::label('username', 'Usuario:', ['class'=>'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('username', NULL, ['class'=>'form-control', 'required']) !!}
        @if ($errors->has('username'))
            <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    {!! Form::label('email', 'E-Mail:', ['class'=>'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::email('email', NULL, ['class'=>'form-control', 'required']) !!}
        @if ($errors->has('email'))
            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
        @endif
    </div>
</div>

<div class="form-group">
    {!! Form::label('groups', 'Roles:', ['class'=>'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('groups[]', $groups, null, ['class'=>'form-control', 'required', 'multiple']) !!}
    </div>
</div>