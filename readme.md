# THE PROJECT

# Contribuir

:+1: :tada: Primero que nada gracias por tomarte el tiempo para contribuir a este proyecto :+1: :tada:

### Antes de hacer tu contribución por favor lee la guía de estilo del código [Guía de estilo del código](https://gitlab.com/ajcarrillo/guia_desarrollo/).
El estilo del código es especialmente importante si estamos en un equipo de desarrollo o si nuestro proyecto lo van a usar en algún momento otros desarrolladores. Pero, cuando trabajamos en un proyecto propio, también es una buena costumbre usar un estilo de código claro y optimizado. Nos ayudará a revisar mejor el código y a entenderlo si en algún momento tenemos que modificarlo o queremos reutilizarlo.

Para contribuir a este proyecto sigue los siguientes pasos:
 
 ```
 $ https://gitlab.com/ajcarrillo/theproject.git
 $ git checkout -b develop
 $ git pull origin develop
 $ git checkout -b devTuNombre
 
 // Despues de hacer tu contribución
 $ git pull origin develop
 // Corrige errores, si los hay
 $ git push origin devTuNombre
 ```
 
 * Clona el proyecto
 * Crea y baja la rama `develop` del proyecto
 * Crea tu rama de trabajo
 * Haz tu contribución
 * Baja los últimos cambios en la rama `develop`, arregla conflictos si los hay
 * Sube tu contribución
 * Y haz un pull request a la rama `develop` del proyecto.
 
## Dependencias

* `node`
* `yarn`

Para instalar `yarn` ejecuta en la terminal:

```
$ npm install --global yarn
```

## :baby_bottle: Baby Steps I

* Renombrar los archivos `/.env.example` a `.env`.
* Ejecutar en la linea de comandos `$ yarn install`.
* Ejecutar en la linea de comandos `$ composer install`
* Ejecutar en la linea de comandos `$ npm run dev`

### Archivo .env

Aquí se encuentra contenidas algunas variables y configuraciones de vital importancia para que el proyecto funcione.

La variable `APP_KEY` se rellena automáticamente ejecutando el comando:

```
$ php artisan key:generate
```

## :baby_bottle: Baby Steps II

### Configuración y creacion de base de datos.

Puedes crear la base de datos ejecutando el siguiente script:

```sql
DROP DATABASE IF EXISTS theproject;
CREATE DATABASE theproject
   CHARACTER SET = 'utf8'
   COLLATE = 'utf8_general_ci';
```

*Nota: una vez creada la base de datos, configura tu conexión*

### Configuración de la conexión

```
DB_CONNECTION=mysql
DB_HOST=YourDatabaseServer
DB_PORT=YourPort
DB_DATABASE=theproject
DB_USERNAME=YourUsername
DB_PASSWORD=YourPassword
```

### Configuración de bases de datos adicionales `plantilla`, `usuarios`, `portal_docente`, `geoestadisticadb`

```
DB_PLANTILLA_DATABASE=plantilla
DB_PLANTILLA_USERNAME=
DB_PLANTILLA_PASSWORD=
```
```
DB_USUARIOS_DATABASE=usuarios
DB_USUARIOS_USERNAME=
DB_USUARIOS_PASSWORD=
```
```
DB_PORTAL_DOCENTE_DATABASE=portal_docentes
DB_PORTAL_DOCENTE_USERNAME=
DB_PORTAL_DOCENTE_PASSWORD=
```
```
DB_GEOESTADISTICADB_DATABASE=geoestadisticadb
DB_GEOESTADISTICADB_USERNAME=
DB_GEOESTADISTICADB_PASSWORD=
```

### Migraciones con laravel

Ejecutar los siguientes comandos para aplicar las migraciones e inicializar la base datos.

```
$ php artisan migrate:fresh --seed
```

# Producción

Es importante cambiar en el archivo `.env` la variable `DEBUGBAR_ENABLED`:

```
DEBUGBAR_ENABLED = false
```

Esto para que la barra de depuración no se muestre.

Happy coding!!! :slight_smile: :upside_down:

P.S. Alimentate sanamente, come frutas y verduras. :tomato: :hot_pepper: :corn: